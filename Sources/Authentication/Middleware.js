let _ = require("lodash");
let fs = require("fs");
let config = JSON.parse(fs.readFileSync("./config.json"));
let router = require("express").Router();
let UserManager = require("./UserManager.js");

let GoogleStrategy = require("passport-google-oauth20").Strategy;
let FacebookStrategy = require("passport-facebook").Strategy;
let TwitterStrategy = require("passport-twitter").Strategy;

module.exports = passport => {

	passport.serializeUser((user, callback) => {
		if (user.id)
			callback(null, user.id);
		else
			callback("invalid user data");
	});

	passport.deserializeUser((id, callback) => {
		UserManager
			.get({ id })
			.then(user => { callback(null, user) })
			.catch(err => { callback(JSON.stringify(err)) })
	});

	passport.use(
		new GoogleStrategy(_.assign(config.auth.google, { "passReqToCallback" : true }),
		(req, accessToken, refreshToken, profile, callback) => {
			UserManager.get({
				"googleId" : profile.id
			})
			.then(user => { callback(null, user) })
			.catch(err => {
				if (req.isAuthenticated()) {
					UserManager.update(req.user.id, {
						"googleId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
				else
				{
					UserManager.create({
						"name"     : profile.displayName,
						"googleId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
			})
		})
	);

	passport.use(
		new FacebookStrategy(_.assign(config.auth.facebook, { "passReqToCallback" : true }),
		(req, accessToken, refreshToken, profile, callback) => {
			UserManager.get({
				"facebookId" : profile.id
			})
			.then(user => { callback(null, user) })
			.catch(err => {
				if (req.isAuthenticated()) {
					UserManager.update(req.user.id, {
						"facebookId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
				else
				{
					UserManager.create({
						"name"       : profile.displayName,
						"facebookId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
			})
		})
	);

	passport.use(
		new TwitterStrategy(_.assign(config.auth.twitter, { "passReqToCallback" : true }),
		(req, token, tokenSecret, profile, callback) => {
			UserManager.get({
				"twitterId" : profile.id
			})
			.then(user => { callback(null, user) })
			.catch(err => {
				if (req.isAuthenticated()) {
					UserManager.update(req.user.id, {
						"twitterId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
				else
				{
					UserManager.create({
						"name"      : profile.displayName,
						"twitterId" : profile.id
					})
					.then(user => { callback(null, user) })
					.catch(err => { callback(JSON.stringify(err)) })
				}
			})
		})
	);

	router
		.get("/user", (req, res) => {
			res.json(req.user || null);
		})
		.get("/logout", (req, res) => {
			req.logout();
			res.redirect("/");
		})
		.get("/:provider/login",
			(req, res, next) => passport.authenticate(req.params.provider, { "scope" : req.params.provider == "facebook" ? "public_profile" : "profile" })(req, res, next),
			(req, res, next) => { res.send() }
		)
		.get("/:provider/callback",
			(req, res, next) => passport.authenticate(req.params.provider, { "successRedirect" : "/", "failureRedirect" : "/login" })(req, res, next),
			(req, res, next) => { res.redirect("/") }
		);

	return router;
}
