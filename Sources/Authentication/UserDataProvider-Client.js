let http = require("superagent");

module.exports = {
	"getCurrent" : () =>
		new Promise(function(resolve, reject) {
			http.get("/auth/user")
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		})
};
