module.exports = (req) => {
	return { "getCurrent" : () => new Promise((resolve, reject) => { resolve(req.user || null) }) }
};
