let fs = require("fs");
let config = JSON.parse(fs.readFileSync("./config.json"));
let db = require("knex")(config.db);

module.exports = {
		"get" : (filter) => new Promise((resolve, reject) => {
			db("users")
				.where(filter)
				.select(["*"])
				.then(rows => {
					if (rows.length == 1) {
						let user = rows[0];

						delete user.googleId;
						delete user.facebookId;
						delete user.twitterId;

						resolve(user);
						return;
					}

					if (rows.length == 0) {
						reject({ "status" : 404, "text" : "Not Found" });
						return;
					}

					reject({ "status" : 500, "text" : "Internal Server Error", "details" : rows });
				 })
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		}),

		"create" : (data) => new Promise((resolve, reject) => {
			db("users")
				.insert(data)
				.returning(["*"])
				.then(rows => {
					let user = rows[0];

					delete user.googleId;
					delete user.facebookId;
					delete user.twitterId;

					resolve(user);
					return;
				})
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		}),

		"update" : (id, data) => new Promise((resolve, reject) => {
			db("users")
				.where({id})
				.update(data)
				.returning(["*"])
				.then(rows => {
					let user = rows[0];

					delete user.googleId;
					delete user.facebookId;
					delete user.twitterId;

					resolve(user);
					return;
				})
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		})

};
