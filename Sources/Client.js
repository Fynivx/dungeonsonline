let Renderer = require("./Presentation/Renderer.jsx");
let UserProvider = require("./Authentication/UserDataProvider.js");
let UI = require("./Presentation/UI.jsx");

window.ShowUrl = url => { UI.show(url, Renderer, UserProvider) };

ShowUrl(location.pathname + location.search);

addEventListener("popstate", e => { ShowUrl(e.target.location.pathname + e.target.location.search) });
