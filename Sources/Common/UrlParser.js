let Route = require("route-parser");
let queryParser = require("qs");

let SearchRoute = new Route("/:table");
let ItemRoute = new Route("/:table/:id");

module.exports = {
	"parse" : url => {
		let splitted = url.split(/\?(.+)?/);
		let path = splitted[0];
		let query = queryParser.parse(splitted[1]);

		if (path === "/")
			return { "page" : "home", query };

		let request = SearchRoute.match(path) || ItemRoute.match(path);

		if (request) {
			request.query = query;
			return request;
		}

		return null;
	}
};
