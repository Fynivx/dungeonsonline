let http = require("superagent");
let qs = require("qs");

module.exports = {
	"take" : (table, id) =>
		new Promise(function(resolve, reject) {
			http.get("/api/" + table + "/" + id)
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		}),

	"takeMany" : (table, query) =>
		new Promise(function(resolve, reject) {
			http.get("/api/" + table + "?" + qs.stringify(query))
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		}),

	"create" : (table, data) =>
		new Promise(function(resolve, reject) {
			http.get("/api/" + table)
				.send(data)
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		}),

	"update" : (table, id, data) =>
		new Promise(function(resolve, reject) {
			http.put("/api/" + table + "/" + id)
				.send(data)
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		}),

	"delete" : (table, id) =>
		new Promise(function(resolve, reject) {
			http.del("/api/" + table + "/" + id)
				.end(function (err, res) {
					if (err) {
						reject(err);
						return;
					}

					resolve(res.body);
				});
		})
};
