let fs = require("fs");
let config = JSON.parse(fs.readFileSync("./config.json"));
let db = require("knex")(config.db);
let inflection = require("inflection");
let linq = require("linq");
let Model = require("./ModelMetadataProvider.js");
let data = JSON.parse(fs.readFileSync("./data.json"));

module.exports = {
	"build" : () =>
		Model.getTables()
			.select(t => db.schema
				.raw(`create table if not exists "${t.name}" (${t.inherits ? `CONSTRAINT ${t.name}_pkey PRIMARY KEY (id)` : ""}) ${t.inherits ? `INHERITS ("${t.inherits}")` : ""}`)
				.table(t.name, table => {
					t.columns
						.forEach(c => {
							let column = table.specificType(c.name, c.type);

							if (c.refs) column = column.references("id").inTable(c.refs);

							column = c.optional ? column.nullable() : column.notNullable();

							if (c.unique) {
								column = column.unique();
							}
						});

					if (t.inherits == undefined)
						table.primary("id");
				})
			).aggregate((t1, t2) => t1.then(() => t2)),

	"clean" : () =>
		Model.getTables()
			.select(t => db.schema
				.raw(`drop table if exists ${t.name} cascade`)
			).aggregate((t1, t2) => t1.then(() => t2)),

	"fill" : () =>
		linq.from(data)
			.select(r => db(r.table)
				.insert(r.row)
			).aggregate((t1, t2) => t1.then(() => t2))
};
