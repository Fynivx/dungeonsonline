let Model = require("./Model.json")
let linq = require("linq");
let _ = require("lodash");
let inflection = require("inflection");


let propToColumn = p => {
	return {
		"name"     : p.name + (p.kind == "link" ? "Id" : ""),
		"type"     : p.kind == "value" ? p.type : "bigint",
		"refs"     : p.kind == "link"  ? inflection.tableize(p.type) : undefined,
		"optional" : p.optional || false,
		"unique"   : p.unique || false
	};
};

let modelToTable = model => {
	let table = {};

	table.name = inflection.tableize(model.name);
	table.abstract = model.abstract;
	table.private = model.private;

	table.columns = linq
		.from(model.props)
		.where(p => p.kind != "synth" && p.array == undefined)
		.select(p => propToColumn(p));

	if (model.inherits) table.inherits = inflection.tableize(model.inherits);

	return table;
};

let propToTable = prop => {
	let table = {};

	table.name = prop.src + "_" + prop.name;

	table.columns = linq
		.from(prop.attrs)
		.select(a => propToColumn(a))
		.union([
			{ "name" : "id"       , "type" : "bigserial" } ,
			{ "name" : "sourceId" , "type" : "bigint"    , "refs" : prop.src },
			{ "name" : "targetId" , "type" : "bigint"    , "refs" : inflection.tableize(prop.type)}
		]);

	table.link = true;

	return table;
}

let propToField = prop => {
	let field = {
		"name"     : inflection.transform(prop.name, ["underscore", "titleize"]),
		"prop"     : prop.name + (prop.kind == "link" ? "Id" : ""),
		"long"     : prop.long || false,
		"optional" : prop.optional || false
	};

	if (prop.kind == "link")
		field.source = {
			"table": inflection.tableize(prop.type),
			"query": {}
		};
	else field.type = prop.type;

	return field;
};

let getTables = () => linq
	.from(Model)
	.select(m => modelToTable(m))
	.union(linq
		.from(Model)
		.selectMany(m => linq.from(m.props).select(p => _.assign(m, { "src" : inflection.tableize(m.name)})))
		.where(p => p.kind == "link" && p.array)
		.select(p => propToTable(p))
	)
;

let getModelByTable = t => linq
	.from(Model)
	.where(m => inflection.tableize(m.name) == t)
	.first()
;

let getModelByName = n => linq
	.from(Model)
	.where(m => m.name == n)
	.first()
;

let getFieldsForTable = t => getFieldsForModel(getModelByTable(t));

let getFieldsForModel = m => {
	let local = linq
		.from(m.props)
		.where(p => !p.array)
		.where(p => p.kind != "synth")
		.where(p => p.filter !== false)
		.where(p => p.hidden != true)
		.select(p => propToField(p));

	if (m.inherits) return local.union(getFieldsForModel(getModelByName(m.inherits)));

	return local;
};

let getNameForTable = t => getModelByTable(t).name;

let getSectionsForTable = t => linq.from([])
	// TODO
;

module.exports = { getTables, getFieldsForTable, getSectionsForTable, getNameForTable };
