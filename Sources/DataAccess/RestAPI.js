let router = require("express").Router();
let bodyParser = require("body-parser");
let UrlParser = require("../Common/UrlParser.js");
let DataStore = require("./DataStore.js");

router
	.use(bodyParser.json())
	.get("*", (req, res, next) => {
		let request = UrlParser.parse(req.url);

		if (request == null) {
			res.status(404).json("Not Found");
			return;
		}

		if (request.id) {
			DataStore
				.take(request.table, request.id)
				.then(item => res.json(item))
				.catch(e => res.status(e.status).json(e));
			return;
		}

		if (request.table) {
			DataStore
				.takeMany(request.table, request.query)
				.then(rows => res.json(rows))
				.catch(e => res.status(e.status).json(e));
			return;
		}

		res.status(404).json("Not Found");
	})
	.post("*", (req, res, next) => {
		let request = UrlParser.parse(req.url);

		if (!req.user) {
			res.status(403).json("Forbidden");
			return;
		}

		if (request == null) {
			res.status(405).json("Method Not Allowed");
			return;
		}

		if (request.id) {
			res.status(405).json("Method Not Allowed");
			return;
		}

		if (request.table) {
			DataStore
				.create(request.table, req.body)
				.then(item => res.json(item))
				.catch(e => res.status(e.status).json(e));
			return;
		}

		res.status(405).json("Method Not Allowed");
	})
	.put("*", (req, res, next) => {
		let request = UrlParser.parse(req.url);

		if (!req.user) {
			res.status(403).json("Forbidden");
			return;
		}

		if (request == null) {
			res.status(405).json("Method Not Allowed");
			return;
		}

		if (request.id) {
			DataStore
				.update(request.table, request.id, req.body)
				.then(item => res.json(item))
				.catch(e => res.status(e.status).json(e));
			return;
		}

		res.status(405).json("Method Not Allowed");
	})
	.delete("*", (req, res, next) => {
		let request = UrlParser.parse(req.url);

		if (!req.user) {
			res.status(403).json("Forbidden");
			return;
		}

		if (request == null) {
			res.status(405).json("Method Not Allowed");
			return;
		}

		if (request.id) {
			DataStore
				.delete(request.table, request.id)
				.then(item => res.json(item))
				.catch(e => res.status(e.status).json(e));
			return;
		}

		res.status(405).json("Method Not Allowed");
	});

module.exports = router;
