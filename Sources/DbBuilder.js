let builder = require("./DataAccess/DbBuilder.js");

builder.clean()
	.then(() => builder.build())
	.then(() => builder.fill())
	.then(() => console.log("All done!"));
