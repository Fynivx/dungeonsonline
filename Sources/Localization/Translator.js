let fs = require("fs");
let config = JSON.parse(fs.readFileSync("./config.json"));
let _ = require("lodash");
let db = require("knex")(_.assign(config.db, { "searchPath" : "public,\"Localization\"" }));
let linq = require("linq");

let data = [];

module.exports = {
		"take" : (table, id) => new Promise((resolve, reject) => {
			let metadata = Model.getTables().where(t => t.name == table).first();

			if (metadata.private)
			{
				reject({ "status" : 403, "text" : "Forbidden" });
				return;
			}

			db(table)
				.where({id})
				.select(["*", db.raw("tableoid::regclass as table")])
				.then(rows => {
					if (rows.length == 1) {
						resolve(rows[0]);
						return;
					}

					if (rows.length == 0) {
						reject({ "status" : 404, "text" : "Not Found" });
						return;
					}

					reject({ "status" : 500, "text" : "Internal Server Error", "details" : "DB returned more than one row" });
				 })
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		}),

		"takeMany" : (table, query) => new Promise((resolve, reject) => {
			let metadata = Model.getTables().where(t => t.name == table).first();

			if (metadata.private)
			{
				reject({ "status" : 403, "text" : "Forbidden" });
				return;
			}

			let request = db(table);

			if (query.q) {
				request = request.whereRaw("\"searchVector\" @@ plainto_tsquery(?)", query.q);
				if (query.where) request = request.andWhere(query.where);
			}
			else if (query.where) request = request.where(query.where);

			request
				.select(["*", db.raw("tableoid::regclass as table")])
				.orderBy(query.order || "id", query.desc ? "desc" : "asc")
				.offset(query.offset || 0)
				.limit(query.limit || 128)
				.then(rows => { resolve(rows) })
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		}),

		"create" : (table, data) => new Promise((resolve, reject) => {
			let metadata = Model.getTables().where(t => t.name == table).first();

			delete data.id;
			delete data.table;

			if (metadata.private)
			{
				reject({ "status" : 403, "text" : "Forbidden" });
				return;
			}

			db(table)
				.insert(data)
				.returning(["*", db.raw("tableoid::regclass as table")])
				.then(rows => {
					resolve(rows[0])
				})
				.catch(e => {
					reject({ "status" : 500, "text" : "Internal Server Error", "details" : e })
				});
		}),

		"update" : (table, id, data) => new Promise((resolve, reject) => {
			let metadata = Model.getTables().where(t => t.name == table).first();

			delete data.id;
			delete data.table;

			if (metadata.private)
			{
				reject({ "status" : 403, "text" : "Forbidden" });
				return;
			}

			db(table)
				.where({id})
				.update(data)
				.returning(["*", db.raw("tableoid::regclass as table")])
				.then(rows => {
					resolve(rows[0])
				})
				.catch(e => {
					reject({ "status" : 500, "text" : "Internal Server Error", "details" : e })
				});
		}),

		"delete" : (table, id) => new Promise((resolve, reject) => {
			let metadata = Model.getTables().where(t => t.name == table).first();

			if (metadata.private)
			{
				reject({ "status" : 403, "text" : "Forbidden" });
				return;
			}

			db(table)
				.where({id})
				.del()
				.returning(["*", db.raw("tableoid::regclass as table")])
				.then(rows => { resolve(rows[0]) })
				.catch(e => { reject({ "status" : 500, "text" : "Internal Server Error", "details" : e }) });
		})
};
