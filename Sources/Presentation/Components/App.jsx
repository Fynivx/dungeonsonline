let React = require("react");

let AppHeader = require("./AppHeader.jsx");
let AppFooter = require("./AppFooter.jsx");
let Sidebar = require("./Sidebar.jsx");

module.exports = class App extends React.Component {
    render() {
        return (
			<div id="app">
                <AppHeader user={this.props.user}/>
                <Sidebar/>
                {this.props.children}
                <AppFooter author={this.props.author}/>
			</div>
        );
    }
};
