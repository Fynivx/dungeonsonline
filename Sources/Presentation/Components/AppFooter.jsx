var React = require("react");

module.exports = class AppFooter extends React.Component {
    render() {
        var year = new Date().getFullYear();

        return (
            <footer>© {year <= 2016 ? "2016" : "2016 - " + year} {this.props.author}<br/>except content, libs, and ads</footer>
        );
    }
};
