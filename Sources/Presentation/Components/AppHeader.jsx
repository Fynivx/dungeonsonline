let React = require("react");
let Link = require("./Link.jsx");
let LoginControl = require("./LoginControl.jsx");
let LogoutLink = require("./LogoutLink.jsx");

module.exports = class AppHeader extends React.Component {
    render() {
        return (
            <header>
            	<Link to="/">Home</Link>
				<Link to="/publishers">Publishers</Link>
				<Link to="/authors">Authors</Link>
				<Link to="/editions">Editions</Link>
				<Link to="/rulebooks">Rulebooks</Link>
				<Link to="/magazines">Magazines</Link>
				<Link to="/magazine_issues">Magazine Issues</Link>
				{this.props.user ? <LogoutLink/> : <LoginControl/>}
            </header>
        );
    }
};
