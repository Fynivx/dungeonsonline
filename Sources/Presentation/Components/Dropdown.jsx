let linq = require("linq");
let React = require("react");
let InputBase = require("./InputBase.jsx");

module.exports = class Dropdown extends InputBase {
	handleInternalChange(params) {
		this.setState({ value : params.target.value });
		this.props.onChange(params.target.value);
	}

	render() {
		let click = this.handleInternalChange.bind(this);

		return (
			<select value={this.state.value} onChange={this.handleInternalChange.bind(this)}>
				{this.props.optional ? <option value={undefined}></option> : ""}
				{this.props.rows.map(r => <option key={r.id} value={r.id}>{r.name}</option>)}
			</select>
		);
	}
};
