let React = require("react");
let Page = require("./Page.jsx");

module.exports = class ErrorPage extends React.Component {
    render() {
    	let message;

    	switch (this.props.status) {
    		case 404:
    			message = "But there is nothing here.";
    			break;

    		default:
    			message = "But something went wrong on our side. We are working hard on solving this issue!";
    	}

        return (
            <Page>
                <header>{this.props.status}</header>
                <section>
                	<header>We are so sorry!</header>
                	{message}
                </section>
            </Page>
        );
    }
};