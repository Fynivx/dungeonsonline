let React = require("react");
let Link = require("./Link.jsx");

module.exports =
class GetButton extends React.Component {
    render() {
        return (
			<Link to = {this.props.to} className = "button">
				{this.props.children}
			</Link>
		);
    }
};
