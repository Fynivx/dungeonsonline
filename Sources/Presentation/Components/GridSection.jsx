let React = require("react");
let ItemPreview = require("./ItemPreview.jsx");

module.exports = class GridSection extends React.Component {
    render() {
        return (
            <section className="grid-section">
                <header>{this.props.title}</header>
				<div className="content">
                	{this.props.rows.map(r => <ItemPreview key={r.id} item={r}/>)}
				</div>
            </section>
        );
    }
};
