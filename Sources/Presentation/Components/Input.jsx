let linq = require("linq");
let React = require("react");
let InputBase = require("./InputBase.jsx");

let Dropdown = require("./Dropdown.jsx");
let TextInput = require("./TextInput.jsx");
let TextArea = require("./TextArea.jsx");
let IntegerInput = require("./IntegerInput.jsx");

module.exports = class Input extends InputBase {
	getInput () {
		let onChange = this.props.onChange;
		let field = this.props.field;
		let value = this.props.value;

		if (field.source) {
			if (field.optional)
				field.source.push({"id" : null, "name" : "<empty>"});

			return <Dropdown value={value} rows={field.source} optional={true} onChange={onChange} />;
		}

		switch (field.type) {
			case "text" :
				return field.long
					? <TextArea  value={value} onChange={onChange} />
					: <TextInput value={value} onChange={onChange} />;

			case "smallint" :
			case "integer"  :
			case "bigint"   :
				return <IntegerInput value={value} onChange={onChange} />;
		}

		throw "Unknown Field Type: " + field.type;
	}

	render() {
		let input = this.getInput();

		return (
			<div className="input">
				<div>{this.props.field.name + ":"}</div>
				{input}
			</div>
		);
	}
};
