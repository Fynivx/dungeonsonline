let React = require("react");
let _ = require("lodash");

module.exports = class InputBase extends React.Component {
	mergeState(state) {
		this.setState(_.assign(this.state, state));
	}

    componentWillReceiveProps(props) {
        this.mergeState({ "value" : (props.value || "") });
    }

    componentWillMount() {
        this.mergeState({ "value" : (this.props.value || "") });
    }
};
