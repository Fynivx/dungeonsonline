let React = require("react");
let InputBase = require("./InputBase.jsx");

module.exports = class IntegerInput extends InputBase {
	handleInternalChange(params) {
		this.setState({ value : params.target.value });
		this.props.onChange(params.target.value);
	}

	render() {
		return (
			<input
				type="number"
				step={1}
				placeholder={this.props.hint}
				value={this.state.value}
				autoComplete="on"
				autoFocus={this.props.autoFocus || false}
				onChange={this.handleInternalChange.bind(this)}
				className={this.props.className} />
		);
	}
};
