let React = require("react");
let Link = require("./Link.jsx");

module.exports = class ItemLink extends React.Component {
    render() {
        return (
			<Link to={"/" + this.props.table + "/" + this.props.id}>{this.props.children}</Link>
        );
    }
};
