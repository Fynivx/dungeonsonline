let React = require("react");
let inflection = require("inflection");
let Page = require("./Page.jsx");
let Input = require("./Input.jsx");
let SaveButton = require("./SaveButton.jsx")

module.exports = class ItemPage extends React.Component {
	componentWillReceiveProps(props) {
        this.setState(_.assign({}, props.item));
    }

    componentWillMount() {
		this.setState(_.assign({}, this.props.item));
    }

    render () {
        return (
            <Page>
            	<header>
					{this.state.id ? (this.state.name || ("Unnamed " + inflection.titleize(this.props.modelName))) : ("New " + this.props.modelName)}
					<SaveButton data={this.state}/>
				</header>
            	<section>
            		<header>Summary</header>
					{this.props.fields.map(f => <Input
						key={f.prop}
						field={f} value={this.state[f.prop]}
						onChange={val => {
							let wrapper = {};
							wrapper[f.prop] = val;

							this.setState(_.assign(this.state, wrapper));
						}}
					/>)}
            	</section>
            </Page>
        );
    }
}
