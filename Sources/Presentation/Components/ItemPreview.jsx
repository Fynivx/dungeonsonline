let React = require("react");
let ItemLink = require("./ItemLink.jsx");

module.exports = class ItemPreview extends React.Component {
    render () {
    	let item = this.props.item;

        return (
			<ItemLink table={this.props.item.table} id={this.props.item.id}>
            <div className="preview">
            	<header>{item.name}</header>
            	<section>
            		<header>Summary</header>
            		{item.summary}
            	</section>
            </div>
			</ItemLink>
        );
    }
}
