let React = require("react");
let RedirectHelper = require("../RedirectHelper.js");

module.exports =
class Link extends React.Component {
    handleClick(e) {
		if (this.props.direct) return true;

		if (e.button == 2) return true;
		if (e.button != 0) return false;

		if (history) {
	        e.preventDefault();

	        RedirectHelper.go(this.props.to);

			return false;
		}

		return true;
    }

    render() {
        return (
			<a href = {this.props.to} onClick = {this.handleClick.bind(this)} className = {this.props.className}>
				{this.props.children}
			</a>
		);
    }
};
