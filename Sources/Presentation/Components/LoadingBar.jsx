var React = require("react");

module.exports = class LoadingBar extends React.Component {
    render() {
        return (
            <div className="loading-bar" dataState={this.props.state} dataInternal={this.props.internal}/>
        );
    }
}
