let React = require("react");
let Link = require("./Link.jsx");

module.exports = class LoginControl extends React.Component {
    render() {
        return (
            <span className="login">
				Login:
            	<Link to="/auth/google/login" direct={true}><img src="/res/google.svg" /></Link>
				<Link to="/auth/facebook/login" direct={true}><img src="/res/facebook.svg" /></Link>
				<Link to="/auth/twitter/login" direct={true}><img src="/res/twitter.svg" /></Link>
            </span>
        );
    }
};
