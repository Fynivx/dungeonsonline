let React = require("react");
let Link = require("./Link.jsx");

module.exports = class LogoutLink extends React.Component {
    render() {
        return (
        	<Link to="/auth/logout" direct={true}>Logout</Link>
        );
    }
};
