let React = require("react");
let GetButton = require("./GetButton.jsx");

module.exports =
class NewButton extends React.Component {
    render() {
        return (
			<GetButton to = {"/" + this.props.table + "/new"}>+</GetButton>
		);
    }
};
