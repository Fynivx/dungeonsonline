var React = require("react");

module.exports = class Overlay extends React.Component {
    render() {
        return (
            <div className="overlay" dataVisible={this.props.visible}>
                {this.props.children}
            </div>
        );
    }
}
