var React = require("react");

module.exports = class Page extends React.Component {
    render() {
        return (
            <main>
                <article>{this.props.children}</article>
            </main>
        );
    }
}
