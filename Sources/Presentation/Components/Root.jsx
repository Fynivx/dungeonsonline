let React = require("react");

module.exports = class Root extends React.Component {
    render () {
        let props = this.props;

        return (
            <html lang = {props.lang}>
                <head>
                    <meta charSet = "UTF-8" />
					<meta name="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

                    <meta name = "application-name" content = {props.appName} />
                    <meta name = "author"           content = {props.author} />

					<meta name = "document-state" content = "dynamic" />
					<meta name = "keywords"       content = {props.keywords.join(",")} />

					<meta name = "description"    content = {props.description} />
					<meta name = "msapplication-tooltip" content = {props.tooltip} />

                    <link rel = "icon"       href = {props.iconUrl} />
                    <link rel = "help"       href = {props.helpUrl} />
                    <link rel = "license"    href = {props.licenseUrl} />
                    <link rel = "search"     href = {props.searchUrl} />

                    <link rel = "stylesheet" href = "/res/style.css" />

                    <title>{props.title || props.appName}</title>
                </head>
                <body>
                    <div id = "appContainer">
                        {props.children}
                    </div>

                    <script src="/res/logic.js"/>
                </body>
            </html>
        );
    }
};
