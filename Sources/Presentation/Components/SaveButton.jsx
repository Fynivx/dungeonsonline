let React = require("react");
let RedirectHelper = require("../RedirectHelper.js");
let DataStore = require("../../DataAccess/DataStore.js");

module.exports =
class SaveButton extends React.Component {
    handleClick(e) {
		if (e.button != 0) return;

		let table = this.props.data.table;
		let data = this.props.data;

		let promise = data.id
			? DataStore.update(table, data.id, data)
			: DataStore.create(table, data);

		promise.then((data) => {
			if (history) history.back();
		});
    }

    render() {
        return (
			<button onClick = {this.handleClick.bind(this)} className = "green">
				{this.props.data.id ? "Save" : "Create"}
			</button>
		);
    }
};
