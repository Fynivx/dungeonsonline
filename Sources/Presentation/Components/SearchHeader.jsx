let inflection = require("inflection");
let linq = require("linq");
let _ = require("lodash");
let qs = require("qs");
let React = require("react");
let RedirectHelper = require("../RedirectHelper.js");
let TextInput = require("./TextInput.jsx");
let Input = require("./Input.jsx");
let NewButton = require("./NewButton.jsx");

module.exports = class SearchHeader extends React.Component {
    componentWillReceiveProps(props) {
        this.setState(_.assign({ "q" : "", "where" : {} }, props.query));
    }

    onQueryChange(newValue) {
		this.setState(_.assign(this.state, { "q" : newValue }));
		this.initiateRedirect();
    }

	initiateRedirect() {
		if (this._searchTimeout) {
            clearTimeout(this._searchTimeout);
            this._searchTimeout = null;
        }

        this._searchTimeout = setTimeout(function () {
			RedirectHelper.go("?" + qs.stringify(this.state));
        }.bind(this), 1000);
	}

    componentWillMount() {
		this.setState(_.assign({ "q" : "", "where" : {} }, this.props.query));
    }

    render() {
        return (
            <header className="search-header">
                {inflection.titleize(this.props.table)}<NewButton table={this.props.table}/><br/>
                <TextInput
					value={this.state.q}
					hint="What do you wanna find? Type here."
					autoFocus={true}
					className="full-width"
					onChange={this.onQueryChange.bind(this)}
				/>
				<details>
					<summary>Advanced search</summary>
					{this.props.filters.map(f => <Input
						key={f.prop}
						field={f} value={this.state.where[f.prop]}
						onChange={val => {
							let where = {};

							if (!val) {
								delete this.state.where[f.prop];
							} else if (val == "<empty>") {
								where[f.prop] = null;
								this.state.where = _.assign(this.state.where, where);
							} else {
								where[f.prop] = val;
								this.state.where = _.assign(this.state.where, where);
							}

							this.setState(this.state);

							this.initiateRedirect();
						}}
					/>)}
				</details>
            </header>
        );
    }
};
