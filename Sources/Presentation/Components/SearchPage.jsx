let inflection = require("inflection");

let React = require("react");

let Page = require("./Page.jsx");
let SearchHeader = require("./SearchHeader.jsx");
let GridSection = require("./GridSection.jsx");

let DataStore = require("../../DataAccess/DataStore.js");


module.exports = class SearchPage extends React.Component
{
    render() {
        return (
            <Page>
                <SearchHeader filters={this.props.filters} table={this.props.table} query={this.props.query}/>
                { (this.props.rows || [] ).length > 0
                    ? <GridSection title={"The most relevant " + inflection.humanize(this.props.table, true) + " for your query:"} rows={this.props.rows}/>
                    : ""
                }
            </Page>
        );
    }
};
