let React = require("react");
let InputBase = require("./InputBase.jsx");

module.exports = class TextArea extends InputBase {
	handleInternalChange(params : any) {
		this.setState({ value : params.target.value });
		this.props.onChanged(params.target.value)
	}

	render() {
		return (
			<textarea autoComplete="on" autoFocus={this.props.autoFocus || false} onChange={this.handleInternalChange.bind(this)} className={this.props.className}>
				{this.state.value}
			</textarea>
		);
	}
};
