let UI = require("./UI.jsx");
let Renderer = require("./Renderer.jsx");
let UserProvider = require("../Authentication/UserDataProvider.js");

module.exports = function (req, res, next) {
	UI.show(req.url, Renderer(res), UserProvider(req));
}
