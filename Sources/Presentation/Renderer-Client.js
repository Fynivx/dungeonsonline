let render = require("react-dom").render;
let appContainer = document.getElementById("appContainer");

module.exports = {
	"render" : (view, rootProps) => {
		document.title = rootProps.title;
		render(view, appContainer);
	}
};
