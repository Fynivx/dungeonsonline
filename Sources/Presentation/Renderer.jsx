let React = require("react");
let Root = require("./Components/Root.jsx");
let render = require("react-dom/server").renderToStaticMarkup;

module.exports = res => { return {
	"render" : (view, rootProps, status) => res
		.status(status)
		.send("<!doctype html>" + render(<Root {...rootProps}>{view}</Root>))
};};
