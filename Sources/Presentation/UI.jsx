let React = require('react');
let linq = require("linq");
let UrlParser = require("../Common/UrlParser.js");
let DataStore = require("../DataAccess/DataStore.js");
let Model = require("../DataAccess/ModelMetadataProvider.js");
let _ = require("lodash");
let inflection = require("inflection");

let App = require("./Components/App.jsx");
let ErrorPage = require("./Components/ErrorPage.jsx");
let HomePage = require("./Components/HomePage.jsx");
let ItemPage = require("./Components/ItemPage.jsx");
let SearchPage = require("./Components/SearchPage.jsx");

let conf = {
	"lang"    : "en",
	"appName" : "Dungeons Online",
	"author"  : "Viachaslau Anufryiuk"
};

let renderError = (renderer, error) => {
	renderer.render(<App author={conf.author}><ErrorPage status={error.status}/></App>, {
		"lang"     : conf.lang,
		"appName"  : conf.appName,
		"author"   : conf.author,
		"keywords" : [],
		"robots"   : ["NOINDEX", "FOLLOW"],
		"title"    : error.status + " - " + conf.appName
	}, error.status);
};

module.exports = {
	"show" : (url, renderer, userProvider) => {
		let request = UrlParser.parse(url)

		if (request == null) {
			renderError(renderer, { "status" : 404 });
			return;
		}

		if (request.page == "home") {
			userProvider.getCurrent()
				.then(user => {
					renderer.render(<App author={conf.author} user={user}><HomePage/></App>, {
						"lang"     : conf.lang,
						"appName"  : conf.appName,
						"author"   : conf.author,
						"keywords" : ["dungeon", "dragon", "play", "online"],
						"robots"   : ["INDEX", "FOLLOW"],
						"title"    : conf.appName
					}, 200);
				})
				.catch(e => { renderError(renderer, e) });

			return;
		}

		if (request.id) {
			let fields = Model.getFieldsForTable(request.table);
			let sections = Model.getSectionsForTable(request.table);
			let modelName = Model.getNameForTable(request.table);

			let promises = [
				Promise.all(fields.select(f =>
					f.source
					? DataStore
						.takeMany(f.source.table, f.source.query)
						.then(rows => _.assign(f, { "source" : rows }))
					: new Promise ((resolve, reject) => { resolve(f) })
				).toArray()),
				Promise.all(sections.select(s =>
					s.source
					? DataStore
						.takeMany(s.source.table, s.source.query)
						.then(rows => _.assign(f, { "source" : rows }))
					: new Promise ((resolve, reject) => { resolve(s) })
				).toArray()),
				userProvider.getCurrent()
			];

			if (request.id != "new") promises.push(DataStore.take(request.table, request.id));

			Promise
				.all(promises)
				.then(results => {
					let fields   = results[0] || [];
					let sections = results[1] || [];
					let user     = results[2] || null;
					let item     = results[3] || null;

					renderer.render(<App author={conf.author} user={user}><ItemPage modelName={modelName} sections={sections} fields={fields} item={item}/></App>, {
						"lang"     : conf.lang,
						"appName"  : conf.appName,
						"author"   : conf.author,
						"keywords" : item.searchVector,
						"robots"   : [ item ? "INDEX" : "NOINDEX", "FOLLOW"],
						"title"    : (item ?
							(modelName + " " + item.name) :
							("New " + modelName)) + " - " + conf.appName
					}, 200);
				})
				.catch(e => { renderError(renderer, e) });

			return;
		}

		if (request.table) {
			let filters = Model.getFieldsForTable(request.table);

			Promise
				.all([
					DataStore.takeMany(request.table, request.query),
					Promise.all(filters.select(f =>
						f.source
						? DataStore
							.takeMany(f.source.table, f.source.query)
							.then(rows => _.assign(f, { "source" : rows }))
						: new Promise ((resolve, reject) => { resolve(f) })
					).toArray()),
					userProvider.getCurrent()
				])
				.then(results => {
					let rows    = results[0] || [];
					let filters = results[1] || [];
					let user    = results[2] || null;

					renderer.render(<App author={conf.author} user={user}><SearchPage table={request.table} filters={filters} rows={rows} query={request.query}/></App>, {
						"lang"     : conf.lang,
						"appName"  : conf.appName,
						"author"   : conf.author,
						"keywords" : linq.from(rows).selectMany(r => r.searchVector).toArray(),
						"robots"   : ["INDEX", "FOLLOW"],
						"title"    : inflection.titleize(request.table) + " - " + conf.appName
					}, 200);
				})
				.catch(e => { renderError(renderer, e) });

			return;
		}

		renderError(renderer, { "status" : 404 });
	}
}
