let fs = require("fs");
let config = JSON.parse(fs.readFileSync("./config.json"));

let express = require("express")();
let compression = require("compression")(config.compression);
let resources = require("serve-static")("./res");
let favicon = require("serve-favicon")("./res/icon.ico");
let session = require("cookie-session")(config.session);
let cookie = require("cookie-parser");
let passport = require("passport");

let api = require("./DataAccess/RestAPI.js");
let ui = require("./Presentation/Middleware.js");
let cacheManifest = require("./Caching/ManifestProvider.js");
let auth = require("./Authentication/Middleware.js")(passport);

let server = require("letsencrypt-express")
	.create({
		"configDir" : "letsencrypt",
		"webrootPath" : "letsencrypt/tmp",
		"approveRegistration" : function (hostname, callback) {
			callback(null, config.https);
		}
	});

server.onRequest = express
    .use(compression)
    .use(favicon) /*
    .use(function(req, res, next) {
    	if (req.secure) next();
    	else res.redirect("https://" + req.hostname + req.originalUrl);
    }) */
    .use("/res", resources)
    .use("/res", function (req, res, next) {
    	res.status(404).send("File Not Found");
    })
	.use("/appCache.manifest", cacheManifest)
	//.use(cookie)
	.use(session)
	.use(passport.initialize())
	.use(passport.session())
	.use("/auth", auth)
    .use("/api", api)
    .use(ui);

server.listen([8888], [8889]);
