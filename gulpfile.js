var gulp         = require("gulp"),
    browserify   = require("browserify"),
    sass         = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    cssnano      = require("gulp-cssnano"),
    cssShorthand = require("gulp-shorthand"),
    concatCss    = require("gulp-concat-css"),
    uglify       = require("gulp-uglify"),
    rsync        = require("gulp-rsync"),
    source       = require('vinyl-source-stream'),
    install      = require("gulp-install"),
    buffer       = require("vinyl-buffer");

var styles = function() {
    return gulp
        .src("Sources/Presentation/Styles/*.scss")
        .pipe(sass())
        .pipe(concatCss("style.css"))
        .pipe(autoprefixer({ "browsers" : [">0%"] }))
        .pipe(cssShorthand())
        .pipe(cssnano())
        .pipe(gulp.dest("Build/res/"));
};

var server = function() {
    return browserify(
        ["Sources/Server.js"],
        {
            "bundleExternal": false,
            "builtins": false,
            "commondir": false,
            "insertGlobals": ["__filename", "__dirname"],
            "browserField": false
        })
        .transform("babelify", {"presets": ["es2015", "react"]})
        .bundle()
        .pipe(source("server.js"))
        .pipe(buffer())
        //.pipe(uglify())
        .pipe(gulp.dest("Build/"));
};

var db_builder = function() {
    return browserify(
        ["Sources/DbBuilder.js"],
        {
            "bundleExternal": false,
            "builtins": false,
            "commondir": false,
            "insertGlobals": ["__filename", "__dirname"],
            "browserField": false
        })
        .transform("babelify", {"presets": ["es2015", "react"]})
        .bundle()
        .pipe(source("gendb.js"))
        .pipe(buffer())
        //.pipe(uglify())
        .pipe(gulp.dest("Build/"));
};

var client = function() {
    return browserify("Sources/Client.js")
        .transform("babelify", {"presets": ["es2015", "react"]})
        .bundle()
        .pipe(source("logic.js"))
        .pipe(buffer())
        //.pipe(uglify())
        .pipe(gulp.dest("Build/res/"));
};

var images = function() {
    return gulp
        .src("Resources/Images/*")
        .pipe(gulp.dest("Build/res/"));
};

var config = function() {
    return gulp
        .src("config.json")
        .pipe(gulp.dest("Build/"));
};

var data = function() {
    return gulp
        .src("data.json")
        .pipe(gulp.dest("Build/"));
};

var deps = function() {
    return gulp
        .src("package.json")
        .pipe(gulp.dest("Build/"))
        .pipe(install({
            production: true
        }));
}

var deploy = function() {
    return gulp
        .src("Build/")
        .pipe(rsync({
            "hostname" : "dungeons.online",
            "username" : "fynivx",
            "shell" : "ssh",
            "port" : 22,
            "root" : "Build/",
            "destination" : "/srv/DungeonsOnline/",
            "recursive": true,
            "incremental": true,
            "progress": true,
            "relative": true,
            "emptyDirectories": true,
            "clean": true,
            "compress": true,
            "exclude": ["letsencrypt", ".bin", "server.log"]
        }));
};

gulp.task("config", config);
gulp.task("styles", styles);
gulp.task("images", images);
gulp.task("client", client);
gulp.task("server", server);
gulp.task("deps", deps);
gulp.task("data", data);
gulp.task("db_builder", ["data"], db_builder);

gulp.task("compile", ["client", "server"]);

gulp.task("bundle", ["config", "styles", "images", "client", "server", "deps", "db_builder"]);
gulp.task("deploy", ["bundle"], deploy);

gulp.task("default", ["deploy"]);
